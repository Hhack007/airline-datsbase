#include "sqlite3.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>


using namespace std;

#define __debugbreak() __asm int 3

int main1() {
	system("chcp 65001");
	sqlite3 *db;
	int err_code = 0;
	err_code = sqlite3_open("MYDB", &db);
	if (err_code != SQLITE_OK) __debugbreak();

	int capa;
	cin >> capa;

	const char *sSqlText = "SELECT Current_Owner FROM Current_Status INNER JOIN Planes ON (Planes.Plane_ID=Current_Status.Plane_ID) WHERE Planes.Capacity >= ?001;";

	sqlite3_stmt  *state;
	for (;;) {
		err_code = sqlite3_prepare(db, sSqlText, -1, &state, &sSqlText);

		if (err_code != SQLITE_OK) __debugbreak();

		err_code = sqlite3_bind_int(state, 1, capa);

		if (err_code != SQLITE_OK) __debugbreak();

		if (!state) break;

		int col_count = sqlite3_column_count(state);

		while ((err_code = sqlite3_step(state)) == SQLITE_ROW) {
			for (int i = 0; i < col_count; i++) {
				printf("%s\t", sqlite3_column_text(state, i));
			}
			printf("\r\n");
		}
		if (err_code != SQLITE_DONE) __debugbreak();
	}
	err_code = sqlite3_finalize(state);
	if (err_code != SQLITE_OK) __debugbreak();

	system("pause");
	return 0;
}